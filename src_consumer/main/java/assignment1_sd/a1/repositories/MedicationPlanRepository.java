package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Integer> {
}
