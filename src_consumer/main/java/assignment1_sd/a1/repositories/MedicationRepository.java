package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

}
