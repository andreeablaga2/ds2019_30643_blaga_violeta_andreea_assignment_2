package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

}
