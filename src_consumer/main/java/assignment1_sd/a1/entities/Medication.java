package assignment1_sd.a1.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="medication")
public class Medication {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "medication_id")
    private Integer medicationID;

    @Column(name="name")
    private String name;

    @Column(name="side_effects")
    @ElementCollection(targetClass=String.class)
    private List<String> sideEffects = new ArrayList<>();

    @Column(name="dosage")
    private String dosage;

    @ManyToMany(mappedBy = "meds")
    private List<MedicationPlan> medplans = new ArrayList<>(); //+met equals

    public Medication() {
    }

    public Medication(Integer medicationID, String name, List<String> sideEffects, String dosage, List<MedicationPlan> medplans) {
        this.medicationID = medicationID;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.medplans = medplans;
    }

    public Medication(Integer medicationID, String name, List<String> sideEffects, String dosage) {
        this.medicationID = medicationID;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Integer getMedicationID() {
        return medicationID;
    }

    public void setMedicationID(Integer medicationID) {
        this.medicationID = medicationID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

//    public List<MedicationPlan> getMedplans() {
//        return medplans;
//    }

    public void setMedplans(List<MedicationPlan> medplans) {
        this.medplans = medplans;
    }
}
