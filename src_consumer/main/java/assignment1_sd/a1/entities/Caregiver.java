package assignment1_sd.a1.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="caregivers")
@PrimaryKeyJoinColumn(name="user_id")
public class Caregiver extends User {
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    @Column(name = "caregiver_id")
//    private Integer caregiverID;

    //cascade = CascadeType.ALL,
    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "caregiver")
    private List<Patient> patients = new ArrayList<>();

    public Caregiver() {
    }

    public Caregiver( Integer id, String username, String password, int type, String name, String birthDate, String gender, String address, List<Patient> patients) {
        super(id,username,password,type,name,birthDate,gender,address);
        this.patients = patients;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
