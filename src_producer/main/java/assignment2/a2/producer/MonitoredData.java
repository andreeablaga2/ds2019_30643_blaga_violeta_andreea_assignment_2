package assignment2.a2.producer;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


public class MonitoredData implements Serializable {
    @JsonProperty("patientID")
    private int patientID;

    @JsonProperty("start")
    private LocalDateTime start;

    @JsonProperty("end")
    private LocalDateTime end;

    @JsonProperty("activity")
    private String activity;

    public MonitoredData( int patientID, LocalDateTime start, LocalDateTime end, String activity) {
        this.patientID = patientID;
        this.start=start;
        this.end=end;
        this.activity=activity;
    }
    public MonitoredData()
    {

    }

    public int getPatientID() {
        return 1;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String toString() {
        return "PatientID: " + patientID + "; Start: " + start + "; End: " + end + "; Activity: " + activity;
    }

    public static List<MonitoredData> citire() {
        String fileName = "C:\\Users\\Andreea\\Desktop\\poli\\4\\SD\\tema2\\activity1.txt";
        List<MonitoredData> data=new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(x->{
                        MonitoredData mon=new MonitoredData();
                        mon.setPatientID(1);
                        String str1 = x.split("		")[0];
                        mon.setStart(LocalDateTime.parse(str1, formatter));
                        String str2 = x.split("		")[1];
                        mon.setEnd(LocalDateTime.parse(str2, formatter));
                        mon.setActivity(x.split("		")[2]);
                        data.add(mon);
                    }
            );

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
