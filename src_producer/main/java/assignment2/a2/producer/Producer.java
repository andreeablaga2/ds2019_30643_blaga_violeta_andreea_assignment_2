package assignment2.a2.producer;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Component
public class Producer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingkey}")
    private String routingKey;

    public void produceMsgFromFile(MonitoredData data) {
        List<MonitoredData> lista = new ArrayList<>();
        lista = data.citire();

        for (MonitoredData m : lista) {
            amqpTemplate.convertAndSend(exchange, routingKey, m);
            System.out.println("Send msg = " + m);
        }
    }

//    public void produceMsg(String msg){
//        amqpTemplate.convertAndSend(exchange, routingKey, msg);
//        System.out.println("Send msg = " + msg);
//    }

    //    public void produceMsgFromFile(MonitoredData data){
//        List<MonitoredData> lista = new ArrayList<>();
//        lista = data.citire();
//        List<MonitoredData> listareguli = new ArrayList<>();
//        for (MonitoredData m : lista) {
////            amqpTemplate.convertAndSend(exchange, routingKey, m);
////            System.out.println("Send msg = " + m);
//            //r1
//            if(m.getActivity().equals("Sleeping")){
//                if(ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>7){
//                    //amqpTemplate.convertAndSend(exchange, routingKey, m);
//                    //listareguli.add(m);
//                    System.out.println(m.getStart()+"-------"+m.getEnd());
//                    System.out.print("           DOARMEEEE");
//                    //System.out.println("Send msg = " + m);
//                }
//            }
//            //r2
//            else if(m.getActivity().equals("Leaving")){
////                System.out.println("asta ii afaara");
////                Timestamp timestamp_end = Timestamp.valueOf(m.getEnd());
////                Timestamp timestamp_start = Timestamp.valueOf(m.getStart());
////                if((timestamp_end.getTime()-timestamp_start.getTime())>12*60*60*1000){
//                if(ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>12){
////                    amqpTemplate.convertAndSend(exchange, routingKey, m);
////                    System.out.println("Send msg = " + m);
//                   // listareguli.add(m);
//                    System.out.println(m.getStart()+"-------"+m.getEnd());
//                    System.out.print("           IESEEEE");
//                }
//            }
//            //r3
//            else if (m.getActivity().equals("Toileting") || m.getActivity().equals("Showering")){
////                System.out.println("asta e la bae");
////                Timestamp timestamp_end = Timestamp.valueOf(m.getEnd());
////                Timestamp timestamp_start = Timestamp.valueOf(m.getStart());
////                if((timestamp_end.getTime()-timestamp_start.getTime())>60*60*1000){
//                if(ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>1){
////                    amqpTemplate.convertAndSend(exchange, routingKey, m);
////                    System.out.println("Send msg = " + m);
//                   // listareguli.add(m);
//                    System.out.println(m.getStart()+"-------"+m.getEnd());
//                    System.out.print("           LA BAIE");
//                }
//            }
//
//        }

}

