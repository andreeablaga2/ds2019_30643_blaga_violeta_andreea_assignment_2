package assignment2.a2;

import assignment2.a2.producer.MonitoredData;
import assignment2.a2.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class A2Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(A2Application.class, args);
	}

	@Autowired
    Producer producer;

    @Override
    public void run(String... args) throws Exception {
       MonitoredData filedata = new MonitoredData();
       producer.produceMsgFromFile(filedata);
    }

}
